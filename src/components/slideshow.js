import React from "react";
import Slideshow from "react-native-slideshow";
import { View } from "native-base";

export default class SlideshowGallery extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      position: 0,
      interval: null,
      dataSource: []
    };
  }

  componentWillReceiveProps(nextProps) {
    // ("nextProps => ", nextProps.data);
    let dataSource = [];
    for (var i = 0; i < nextProps.data.length; i++) {
      let item = nextProps.data[i];
      item.url = item.picture;
      dataSource.push(item);
    }
    this.setState({ dataSource });
  }

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position:
            this.state.position === this.state.dataSource.length - 1
              ? 0
              : this.state.position + 1
        });
      }, 2000)
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  render() {
    return (
      <Slideshow
        dataSource={this.state.dataSource}
        position={this.state.position}
        onPositionChanged={position => this.setState({ position })}
        height={200}
        arrowLeft={<View />}
        arrowRight={<View />}
      />
    );
  }
}
