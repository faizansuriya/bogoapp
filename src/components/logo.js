import React from "react";
import { Thumbnail } from "native-base";
import { Assets } from "../../assets";

const Logo = ({ style = {}, ...props }) => {
  return (
    <Thumbnail
      square
      style={{
        width: 160,
        height: 68
      }}
      resizeMode="contain"
      source={Assets.logo}
      {...props}
    />
  );
};

export default Logo;
