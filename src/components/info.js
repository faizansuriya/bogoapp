import React, { Component } from "react";
import { View, Text, StyleSheet, Image, ScrollView } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class StoreInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datasource: {
        outlet_information: {
          id: 1,
          address: "252 Takbeer Block,Bharia Town, Lahore",
          phone_number: "03343283898",
          fax_number: "03433147598"
        },
        outlet_description: {
          description: "This is the shop description."
        },
        specialities: "Subs,Panini,Drinks etc ",
        facilities: "Wifi, Dinein, Parking, Card Accepted"
      }
    };
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={{ backgroundColor: "#E7E7E7" }}>
          <View style={{ marginBottom: 10 }}>
            <View style={styles.row}>
              <Text style={{ fontSize: 16, paddingBottom: 5 }}>
                OUTLET INFORMATION
              </Text>
            </View>
            <View style={styles.row}>
              <Text>
                {/* <Icon name="map-marker" size={22} color="red" /> */}
                {this.state.datasource.outlet_information.address}
              </Text>
            </View>
            <View style={styles.row}>
              <Text>
                {/* <Icon name="phone" size={22} color="red" /> */}
                {this.state.datasource.outlet_information.phone_number}
              </Text>
            </View>
          </View>
        </View>
        <View style={{ backgroundColor: "#E7E7E7" }}>
          <View style={{ marginBottom: 10 }}>
            <View style={styles.row}>
              <Text style={{ fontSize: 14, paddingBottom: 5, color: "red" }}>
                DESCRIPTION
              </Text>
            </View>
            <View style={styles.row}>
              <Text>
                {/* <Icon name="map-marker" size={22} color="red" /> */}
                {this.state.datasource.outlet_description.description}
              </Text>
            </View>
          </View>
        </View>
        <View style={{ backgroundColor: "#E7E7E7" }}>
          <View style={{ marginBottom: 10 }}>
            <View style={styles.row}>
              <Text style={{ fontSize: 14, paddingBottom: 5, color: "red" }}>
                SPECIALITIES
              </Text>
            </View>
            <View style={styles.row}>
              <Text>
                {/* <Icon name="map-marker" size={22} color="red" /> */}
                {this.state.datasource.specialities}
              </Text>
            </View>
          </View>
        </View>
        <View style={{ backgroundColor: "#E7E7E7" }}>
          <View style={{ marginBottom: 10 }}>
            <View style={styles.row}>
              <Text style={{ fontSize: 14, paddingBottom: 5, color: "red" }}>
                FACILITIES
              </Text>
            </View>
            <View style={styles.row}>
              <Text>
                {/* <Icon name="map-marker" size={22} color="red" /> */}
                {this.state.datasource.facilities}
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    backgroundColor: "#ffffff",
    padding: 15,
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: "#f7f7f7"
  }
});
