import React from "react";
import { StyleSheet, TextInput, Image } from "react-native";
import {
  View,
  Container,
  Item,
  Text,
  Content,
  Icon,
  Input,
  Thumbnail,
  Label,
  Form
} from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import MainBackground from "../components/MainBackground";
import Logo from "../components/logo";
import { Colors } from "../styles/colors";
import Autocomplete from "react-native-autocomplete-input";
import { String } from "../utils/strings";
import { Assets } from "../../assets";
import * as userUtil from "../utils/user.util";

class InstituteInformation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      instituteData: {}
    };
  }
  // findFilm(institute) {
  //   if (institute === "") {
  //     return [];
  //   }
  // }

  async componentDidMount() {
    const instituteData = await userUtil.getUserInfo();
    this.setState({ instituteData });
    // ("This is the institute info", instituteData);
  }

  render() {
    // const { institute } = this.state;
    // const names = this.findFilm(institute);
    return (
      <Container style={{ backgroundColor: Colors.white }}>
        <Content>
          <View style={styles.mainContainer}>
            <View style={styles.bottomcontainer}>
              <Form>
                <Item style={{ marginTop: 10 }}>
                  {/* <Text style={{marginRight:20}}> Institute Name:  </Text  > */}
                  <Label> Institute Name: </Label>
                  <Input
                    multiline
                    style={[styles.input, (marginTop = 50)]}
                    value={this.state.instituteData.institute}
                    disabled={true}
                  />
                </Item>

                <Item style={{ marginTop: 20 }}>
                  <Label>Year Of Study: </Label>
                  <Input
                    style={styles.input}
                    value={
                      this.state.instituteData.studyYear &&
                      this.state.instituteData.studyYear.toString()
                    }
                    disabled={true}
                  />
                </Item>

                <Item style={{ marginTop: 20 }}>
                  <Label>Course Length: </Label>

                  <Input
                    style={styles.input}
                    value={
                      this.state.instituteData.courseLength &&
                      this.state.instituteData.courseLength.toString()
                    }
                    disabled={true}
                  />
                </Item>
              </Form>
            </View>

            <View
              style={{
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  flex: 1,
                  marginLeft: 55,
                  marginTop: 10,
                  alignItems: "center"
                }}
              >
                <Thumbnail
                  square
                  //  resizeMode="contain"
                  source={{
                    uri:
                      this.state.instituteData.idCard &&
                      this.state.instituteData.idCard.front
                  }}
                  style={styles.uploadImage}
                />

                <Text
                  style={{
                    // paddingTop: 10,
                    // paddingBottom: 10,
                    textAlign: "center",
                    justifyContent: "center",
                    color: "black",
                    marginRight: 60,
                    marginTop: 10
                  }}
                >
                  Front ID Card
                </Text>
              </View>

              <View
                style={{
                  flex: 1,
                  marginRight: 55,
                  alignItems: "center",
                  marginTop: 10
                }}
              >
                <Thumbnail
                  square={true}
                  // resizeMode="contain"
                  source={{
                    uri:
                      this.state.instituteData.idCard &&
                      this.state.instituteData.idCard.back
                  }}
                  style={styles.SecondImage}
                />

                <Text
                  style={{
                    // paddingTop: 10,
                    // paddingBottom: 10,
                    textAlign: "center",
                    justifyContent: "center",
                    color: "black",
                    marginLeft: 60,
                    marginTop: 10
                  }}
                >
                  Back ID Card
                </Text>
              </View>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center"
    // alignItems: "center",
  },

  topcontainer: {
    justifyContent: "center",
    paddingLeft: 30,
    paddingRight: 30,
    marginTop: 20
  },

  bottomcontainer: {
    // flex: 1,
    // borderTopLeftRadius: 40,
    // borderTopRightRadius: 40,
    paddingTop: 25,
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: Colors.white
  },

  logocontainer: {
    alignItems: "center",
    marginBottom: 20,
    marginTop: 10
  },

  autocompleteContainer: {
    width: "100%",
    display: "flex"
  },
  itemText: {
    fontSize: 15,
    margin: 2
  },
  icons: {
    color: Colors.black
  },
  input: {
    color: Colors.black,
    fontSize: 15
    //paddingLeft:10,
  },
  itemStyle: {
    marginTop: 20,
    flex: 1,
    flexDirection: "row",
    margin: 40
  },
  uploadImage: {
    borderRadius: 20,
    width: 100,
    height: 100,
    marginRight: 80,
    marginTop: 30
  },
  SecondImage: {
    width: 100,
    height: 100,
    borderRadius: 20,
    marginLeft: 60,
    marginTop: 30
  }
});

export default InstituteInformation;
