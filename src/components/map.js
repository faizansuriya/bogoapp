import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
// import MapView from "react-native-maps";

export default class Map extends Component {
  map = null;

  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: 48.85837009999999,
        longitude: 2.2944813000000295,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      marker: {
        title: "Burger Labs",
        address: "21 bis rue de la trippe, paris",
        coord: {
          latitude: 48.85837009999999,
          longitude: 2.2944813000000295
        }
      }
    };
  }
  render() {
    return (
      <View style={styles.container}>
        {/*<MapView style={styles.map} region={this.state.region}>
          <MapView.Marker
            title={this.state.marker.title}
            description={this.state.marker.address}
            coordinate={this.state.marker.coord}
          />
        </MapView>*/}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: "flex-end",
    alignItems: "center"
  },

  map: {
    flex: 1,
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  }
});
