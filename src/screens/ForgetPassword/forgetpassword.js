import React from "react";
import {
  Container,
  Content,
  View,
  Left,
  Right,
  Button,
  Icon,
  Item,
  Input,
  Text,
  Form,
  Thumbnail,
  Label
} from "native-base";
import Navbar from "../../components/navbar";
import {
  StyleSheet,
  AsyncStorage,
  TouchableOpacity,
  Image
} from "react-native";
import Logo from "../../components/logo";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import Snackbar from "react-native-snackbar";
import MainBackground from "../../components/MainBackground";
import services, { updateHeaders } from "../../services";
import * as authUtil from "../../utils/auth.util";
import { get } from "lodash";
import { emailConstraint, passwordConstraint } from "../../utils/constraints";
var validate = require("validate.js");
import { String } from "../../utils/strings";
import { Colors } from "../../styles/colors";
import { StackActions, NavigationActions } from "react-navigation";
import { Assets } from "../../../assets";

class ForgetPassword extends React.Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();
    this.state = {
      code: "",
      email: "",
      password: "",
      confirmPassword: "",
      receivedCode: false,
      buttonTitle: "Send Code"
    };
  }

  async getCode() {
    this.setState({ receivedCode: true, buttonTitle: "Change Password" });

    if (this.state.receivedCode) {
      this.verify();
      return;
    }
    return;
    var constraints = {
      email: {
        presence: { allowEmpty: false },
        email: true,
        exclusion: {
          message: "Please enter a valid email"
        }
      }
    };
    var validation = validate(
      {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        password: this.state.confirmPassword
      },
      constraints,
      { format: "flat" }
    );
    if (validation) {
      // ("Here i am");
      alert(validation[0]);
      return;
    }
    if (this.state.password !== this.state.confirmPassword) {
      alert("Passwords do not match");
      return;
    }
    const params = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    };
  }

  async verify() {
    this.props.navigation.pop();
    return;
    var constraints = {
      email: {
        presence: { allowEmpty: false },
        email: true,
        exclusion: {
          message: "Please enter a valid email"
        }
      },
      password: {
        presence: true,
        length: {
          minimum: 6,
          message: "must be at least 6 characters"
        }
      }
    };
    var validation = validate(
      {
        code: this.state.code,
        email: this.state.email,
        password: this.state.password,
        password: this.state.confirmPassword
      },
      constraints,
      { format: "flat" }
    );

    if (validation) {
      alert(validation[0]);
      return;
    }
    if (this.state.password !== this.state.confirmPassword) {
      alert("Passwords do not match");
      return;
    }
  }

  render() {
    return (
      <MainBackground>
        <TouchableOpacity
          style={{ marginLeft: 10, marginTop: 10 }}
          onPress={() => this.props.navigation.pop()}
        >
          <Icon
            name="ios-arrow-back"
            size={28}
            style={{ color: "white", marginTop: 30 }}
          />
        </TouchableOpacity>
        <KeyboardAwareScrollView contentContainerStyle={styles.scrollView}>
          <View style={styles.logocontainer}>
            <Logo />
          </View>
          <View style={styles.bottomcontainer}>
            <Form style={styles.formStyle}>
              <Icon size={20} name="ios-mail" style={styles.white} />
              <Item floatingLabel>
                <Label style={styles.white}>Enter your email address</Label>
                <Input
                  style={{ color: Colors.white }}
                  editable={!this.state.receivedCode}
                  keyboardType="email-address"
                  autoCapitalize="none"
                  onChangeText={value => this.setState({ email: value })}
                />
              </Item>
            </Form>
            {!this.state.receivedCode ? null : (
              <>
                <Form style={styles.formStyle}>
                  <Icon size={20} name="ios-lock" style={styles.white} />
                  <Item floatingLabel>
                    <Label style={styles.white}>
                      Enter verification code here
                    </Label>
                    <Input style={styles.white} keyboardType="email-address" />
                  </Item>
                </Form>
                <Form style={styles.formStyle}>
                  <Icon size={20} name="ios-lock" style={styles.white} />
                  <Item floatingLabel>
                    <Label style={styles.white}>Enter new password</Label>
                    <Input
                      style={{ color: Colors.white }}
                      secureTextEntry={true}
                      onChangeText={value => this.setState({ password: value })}
                    />
                  </Item>
                </Form>
                <Form style={styles.formStyle}>
                  <Icon size={20} name="ios-lock" style={styles.white} />
                  <Item floatingLabel>
                    <Label style={styles.white}>Confirm new password</Label>
                    <Input
                      style={{ color: Colors.white }}
                      secureTextEntry={true}
                      onChangeText={value =>
                        this.setState({ confirmPassword: value })
                      }
                    />
                  </Item>
                </Form>
              </>
            )}

            <Button style={styles.button} onPress={() => this.getCode()}>
              <Text style={styles.buttonText}> {this.state.buttonTitle}</Text>
            </Button>
          </View>
        </KeyboardAwareScrollView>
      </MainBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  imageStyle: {
    width: 150,
    height: 150,
    alignSelf: "center"
  },
  footerContainer: {
    flexDirection: "row",
    marginBottom: 30
  },

  bottomcontainer: {
    flex: 1,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingTop: 25,
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: Colors.formBackground
  },

  iconStyle: {
    height: 20,
    width: 20
  },
  formStyle: {
    alignSelf: "center",
    flexDirection: "row",
    // width: "90%",
    alignItems: "flex-end",
    justifyContent: "center",
    marginBottom: 10
  },
  scrollView: {
    marginTop: 10,
    // paddingHorizontal: 40,
    flexWrap: "wrap",
    position: "absolute",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "100%",
    height: "100%"
  },
  button: {
    marginTop: 20,
    alignSelf: "stretch",
    height: 45,
    borderRadius: 10,
    backgroundColor: Colors.buttonBackground,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.maroonColor,
    fontWeight: "bold",
    fontSize: 16
  },
  white: {
    color: "white"
  },
  logocontainer: {
    marginBottom: 10,
    alignSelf: "center"
  }
});

export default ForgetPassword;
