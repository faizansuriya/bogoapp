import React, { Component } from "react";
import {
  Container,
  Body,
  View,
  Text,
  Left,
  Right,
  Button,
  Icon,
  Card,
  CardItem,
  Thumbnail
} from "native-base";
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList
} from "react-native";
import _ from "lodash";
import { Colors } from "../../styles/colors";
import Navbar from "../../components/navbar";
import UniversityGuideTabView from "../../components/universityguidetabview";
import Logo from "../../components/logo";
import { Assets } from "../../../assets";
import Icons from "react-native-vector-icons/Entypo";
import { String } from "../../utils/strings";
import services from "../../services";

class UniversityGuideDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      infoUni: [],
      info: {
        id: 1,
        title: "Aga Khan University",
        description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        avatar:
          "http://www.careersinafrica.com/wp-content/uploads/sites/2/2015/09/agakhan.png",
        ranking: 7,
        location: "Karachi",
        images: [
          {
            id: 1,
            avatar:
              "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Aga_Khan_University_.jpg/220px-Aga_Khan_University_.jpg"
          },
          {
            id: 2,
            avatar: "https://www.aku.edu/news/PublishingImages/_762_.jpg"
          },
          {
            id: 3,
            avatar:
              "https://c.tribune.com.pk/2016/04/1083560-AKU-1460525968-616-640x480.jpg"
          }
        ]
      }
    };
  }

  componentDidMount() {
    const getData = this.props.navigation.state.params.info;
    this.setState({ data: getData }, () => {
      console.log("THis is the selected uni data", this.state.data);
    });
  }

  // _getUniData = async () => {
  //   const getData = await services.auth.getAllUniversities();
  //   // ("this is faizan data", getData);
  //   this.setSztate({ infoUni: getData.data });
  //   // ("this is uni data", this.state.infoUni);
  // };

  //  async _getUniData () {
  //    try{
  //   const getData = await services.auth.getAllUniversities(this.props.navigation.state.params.item);
  //   // // ("this is faizan data", getData.data.name);
  //   this.setState({infoUni:getData.data})
  //   // ("this is uni data", this.state.infoUni)
  // }
  //   catch(error)
  //   {
  //      // .error;
  //   }

  // };

  _renderItem = (item, key) => {
    console.log("This is the image item new", item);
    return (
      <TouchableOpacity
        style={{
          alignItems: "center",
          // backgroundColor: "red",
          flexWrap: "wrap"
        }}
        // onPress={() => this.props.itemPressed(this.props.item)}
      >
        <Image
          source={{ uri: item.item }}
          style={{
            margin: 5,
            width: 100,
            height: 100,
            borderRadius: 10
          }}
        />
      </TouchableOpacity>
    );
  };

  render() {
    const details = this.props.navigation.state.params.info;
    console.log("This is  the complete university details", details);
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
        </Button>
      </Left>
    );
    var right = (
      <Right>
        <Button onPress={() => alert("This is the notification")} transparent>
          <Icon name="ios-notifications" style={{ color: Colors.black }} />
        </Button>
      </Right>
    );
    return (
      <Container>
        <Navbar left={left} title="University Guide" />
        <ScrollView>
          <View style={styles.container}>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Image
                source={{ uri: details.logo }}
                style={{ width: 130, height: 130 }}
              />
            </View>

            <View style={{ height: 60 }}>
              <Text style={{ textAlign: "center", fontSize: 20 }}>
                {/* {this.state.infoUni && this.state.infoUni[0] && this.state.infoUni[0].name } */}
                {details.name}
                {/* {this.props.navigation.state.infoUni.name} */}
              </Text>
            </View>

            {/* <View>
              <Thumbnail
                style={{
                  width: "100%",
                  height: 200
                }}
                // source={{ uri: details.avatar }}
                source={{ uri: details.avatar }}
              />
            </View> */}

            <View
              style={{
                borderBottomColor: Colors.darkGrey,
                borderBottomWidth: 1,
                marginBottom: 10,
                marginTop: 10
              }}
            />
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginBottom: 10
              }}
            >
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{}}>
                  <Icons
                    name="star-outlined"
                    size={20}
                    style={{ color: Colors.cardColor }}
                  />
                </View>
                <View>
                  <Text> Ranking: 7</Text>
                </View>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={{}}>
                  <Icons
                    name="location-pin"
                    size={20}
                    style={{ color: Colors.cardColor }}
                  />
                </View>
                <View>
                  <Text>{details.city}</Text>
                </View>
              </View>
            </View>
            <View>
              <Text>{details.about}</Text>
            </View>
            <FlatList
              style={{ width: "100%", paddingTop: 5 }}
              data={details.media}
              // data={this.state.infoUni}
              renderItem={item => this._renderItem(item)}
              // keyExtractor={(item, index) => item.avatar}
              keyExtractor={(item, index) => item.id}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            />
            {/* , flex:1,flexDirection:"column",justifyContent:'space-between' */}

            <View>
              <UniversityGuideTabView score={details.score} />
            </View>
            <View>
              <Text>Type Of Qualifiation:</Text>
              <View style={{ flex: 1, flexDirection: "row" }}>
                {details.degrees.map((degrees, index) => (
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 12 }}>{degrees}</Text>
                  </View>
                ))}
                <View>
                  <Thumbnail
                    square
                    source={Assets.universityIcon}
                    style={styles.itemImage}
                  />
                </View>
              </View>
            </View>
            <View>
              <Text>Address:</Text>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 12 }}>{details.address}</Text>
                </View>
                <View>
                  <Text style={{ fontSize: 12 }}>Phone {details.phone}</Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  itemBlock: {
    flexDirection: "row",
    paddingBottom: 5,
    flex: 1
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 20
  },
  itemMeta: {
    marginLeft: 10,
    justifyContent: "center",
    flex: 1
  },
  itemName: {
    fontSize: 16,
    fontWeight: "bold",
    color: Colors.cardColor
  },
  itemLastMessage: {
    fontSize: 12,
    color: Colors.black
  },
  separator: {
    height: 0.5,
    width: "80%",
    alignSelf: "center",
    backgroundColor: "#555"
  },
  header: {
    padding: 10
  },
  headerText: {
    fontSize: 30,
    fontWeight: "900"
  },
  button: {
    marginTop: 20,
    width: 250,
    height: 45,
    borderRadius: 50,
    backgroundColor: Colors.cardColor,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.white,
    fontWeight: "bold",
    fontSize: 16
  }
});

export default UniversityGuideDetail;
