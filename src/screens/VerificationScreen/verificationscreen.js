import React from "react";
import {
  Container,
  View,
  Left,
  Right,
  Button,
  Icon,
  Item,
  Input,
  Text
} from "native-base";
import { StyleSheet, TouchableOpacity } from "react-native";
// import {
//   View,
//   Text,
//   StyleSheet,
//   TextInput,
//   TouchableOpacity,
//   KeyboardAvoidingView
// } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import MainBackground from "../../components/MainBackground";
import Logo from "../../components/logo";
import Snackbar from "react-native-snackbar";
import { verifyConstraint } from "../../utils/constraints";
var validate = require("validate.js");
import { String } from "../../utils/strings";
import { Colors } from "../../styles/colors";
import services, { updateHeaders } from "../../services";
import * as authUtil from "../../utils/auth.util";
import * as userUtil from "../../utils/user.util";

class Verification extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      smsCode: ""
    };
  }
  _submit = async () => {
    var constraints = {
      Verification: verifyConstraint
    };
    var validation = validate(
      {
        Verification: this.state.smsCode
      },
      constraints,
      { format: "flat" }
    );
    //   // (validation);
    if (validation) {
      Snackbar.show({
        title: validation[0],
        duration: Snackbar.LENGTH_LONG
      });
      return;
    }
    try {
      const { data } = await services.auth.verifyCode(this.state);
      // (data);
      await userUtil.setUserInfo(data);
      this.props.navigation.navigate("instituteScreen");
    } catch (error) {
      Snackbar.show({
        title: error.response.data.message,
        duration: Snackbar.LENGTH_LONG
      });
      // (error.response);
    }
  };
  render() {
    return (
      <MainBackground>
        <KeyboardAwareScrollView>
          <Container style={{ backgroundColor: "transparent" }}>
            {/* <Navbar left={left} right={right} title="LOGIN" /> */}
            <View style={styles.mainContainer}>
              <View style={styles.topcontainer}>
                <View style={styles.logocontainer}>
                  <Logo />
                </View>
                <View style={{ marginBottom: 25 }}>
                  <Text style={styles.mainTitle}>Verify Now,</Text>
                  <Text style={styles.subTitle}>
                    Enter the verification code sent on your registered number
                  </Text>
                </View>
              </View>
              <View style={styles.bottomcontainer}>
                <Item>
                  <Icon active name="ios-lock" style={styles.icons} />
                  <Input
                    style={styles.input}
                    placeholder={String.verify}
                    onChangeText={text => this.setState({ smsCode: text })}
                    placeholderTextColor={Colors.placeholderColor}
                  />
                </Item>

                <View
                  style={{
                    alignSelf: "stretch"
                  }}
                >
                  <TouchableOpacity
                    onPress={this._submit}
                    style={styles.button}
                  >
                    <Text style={styles.buttonText} uppercase={false}>
                      {String.submit}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Container>
        </KeyboardAwareScrollView>
      </MainBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center"
    // alignItems: "center",
  },

  topcontainer: {
    justifyContent: "center",
    paddingLeft: 30,
    paddingRight: 30,
    marginTop: 20
  },

  bottomcontainer: {
    flex: 1,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingTop: 25,
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: Colors.formBackground
  },
  mainTitle: {
    fontSize: 26,
    fontWeight: "normal",
    textAlign: "left",
    width: "100%",
    color: Colors.title
  },

  subTitle: {
    fontSize: 18,
    textAlign: "left",
    width: "100%",
    color: Colors.subTitle
  },

  smallcontent: {
    fontSize: 16,
    textAlign: "left",
    width: "100%",
    color: Colors.content
  },
  logocontainer: {
    marginBottom: 30,
    marginTop: 10
  },

  icons: {
    color: Colors.white
  },

  input: {
    color: Colors.white
  },

  button: {
    marginTop: 20,
    alignSelf: "stretch",
    height: 45,
    borderRadius: 10,
    backgroundColor: Colors.buttonBackground,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.buttonText,
    fontWeight: "bold",
    fontSize: 16
  }
});

export default Verification;
