import React from "react";
import {
  Container,
  Content,
  View,
  Left,
  Right,
  Button,
  Icon,
  Item,
  Input,
  Text,
  Textarea,
  Thumbnail
} from "native-base";
import {
  StyleSheet,
  Image,
  AsyncStorage,
  TouchableOpacity,
  TextInput
} from "react-native";
import ImagePicker from "react-native-image-picker";
import Logo from "../../components/logo";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import Snackbar from "react-native-snackbar";
import MainBackground from "../../components/MainBackground";
import services, { updateHeaders } from "../../services";
import * as authUtil from "../../utils/auth.util";
import { get } from "lodash";
import { emailConstraint, passwordConstraint } from "../../utils/constraints";
var validate = require("validate.js");
import { String } from "../../utils/strings";
import { Colors } from "../../styles/colors";
import Navbar from "../../components/navbar";
import { StackActions, NavigationActions } from "react-navigation";
import { Assets } from "../../../assets";

const options = {
  title: "Select Image",
  //customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

class AddForum extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      text: "",
      media: ""
    };
  }

  _updateImage = async () => {
    try {
      const formData = new FormData();
      formData.append("file", {
        uri: this.state.media.uri,
        name: `${new Date().getTime()}`,
        type: "image/jpeg"
      });
      const getData = await services.auth.getImageUrl(formData);
      this._postSubmit(getData.data.path);
      // this.setState({ data: getData.data, selectedCategory: getData.data[0] });
    } catch (error) {
      // (error);
    }
  };

  _postSubmit = async path => {
    const payload = {
      media: path,
      title: this.state.title,
      text: this.state.text,
      thumb: path,
      mediaType: "image"
    };
    // ("This is the Payload data", payload);
    try {
      const getData = await services.auth.addPost(payload);

      this.props.navigation.state.params.refreshList();

      this.props.navigation.navigate("DiscussionScreen");
    } catch (error) {
      // (error);
    }
  };

  _uploadImage = image => {
    ImagePicker.showImagePicker(options, response => {
      // ("Response = ", response);

      if (response.didCancel) {
        // ("User cancelled image picker");
      } else if (response.error) {
        // ("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        // ("User tapped custom button: ", response.customButton);
      } else {
        const data = response;
        // ("This is image complete Data", data);
        const source = { uri: response.uri };
        // ("This is media image source>>>", source);

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          media: source
        });
      }
    });
  };

  render() {
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.cardColor }} />
        </Button>
      </Left>
    );
    return (
      <Container>
        <Navbar left={left} title="Add Forum" />
        <KeyboardAwareScrollView>
          <View style={styles.mainContainer}>
            <View style={{ marginBottom: 30 }}>
              <Item>
                <Input
                  style={styles.input}
                  placeholder={String.forumTitle}
                  onChangeText={text => this.setState({ title: text })}
                  placeholderTextColor={Colors.darkGrey}
                  autoCorrect={true}
                  keyboardType="default"
                />
              </Item>
            </View>
            <View style={styles.textAreaContainer}>
              <TextInput
                style={styles.textArea}
                placeholder={String.forumDescription}
                onChangeText={text => this.setState({ text: text })}
                placeholderTextColor={Colors.darkGrey}
                numberOfLines={10}
                multiline={true}
                keyboardType="default"
              />
            </View>
            <Text
              style={{
                paddingTop: 20,
                color: Colors.black
              }}
            >
              Upload Image
            </Text>
            <View
              style={{
                flexDirection: "row"
              }}
            >
              {this.state.media ? (
                <TouchableOpacity
                  style={styles.uploadImage}
                  onPress={this._uploadImage}
                >
                  <Image source={this.state.media} style={styles.uploadImage} />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={styles.uploadImage}
                  onPress={this._uploadImage}
                >
                  <Image source={Assets.postImage} style={styles.uploadImage} />
                </TouchableOpacity>
              )}
            </View>

            <View
              style={{
                alignSelf: "stretch"
              }}
            >
              <TouchableOpacity
                onPress={this._updateImage}
                style={styles.button}
              >
                <Text style={styles.buttonText} uppercase={false}>
                  {String.submit}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    // alignItems: "center",
    paddingLeft: 30,
    paddingRight: 30
  },
  mainTitle: {
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "left",
    width: "100%",
    color: Colors.white
  },

  subTitle: {
    fontSize: 18,
    textAlign: "left",
    width: "100%",
    color: Colors.white
  },

  icons: {
    color: Colors.white
  },

  uploadImage: {
    width: 150,
    height: 100,
    borderRadius: 10
  },

  input: {
    color: Colors.black
  },
  logocontainer: {
    marginBottom: 50,
    alignItems: "center"
  },

  button: {
    marginTop: 20,
    alignSelf: "stretch",
    height: 45,
    borderRadius: 10,
    backgroundColor: Colors.buttonText,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.buttonBackground,
    fontWeight: "bold",
    fontSize: 16
  },

  linktext: {
    width: 200,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15
  },

  textstyle: {
    color: Colors.white,
    fontWeight: "bold",
    fontSize: 16
  },
  textAreaContainer: {
    borderColor: Colors.placeholderColor,
    borderWidth: 1,
    padding: 5,
    height: 150,
    marginBottom: 10
  },
  textArea: {
    color: Colors.black,
    textAlignVertical: "top"
  }
});

export default AddForum;
