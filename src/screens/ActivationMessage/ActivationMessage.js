import React, { Component } from "react";
import { View, Container, Thumbnail, Button } from "native-base";
import Text from "../../components/text";
import { StyleSheet } from "react-native";
import { Assets } from "../../../assets";
import { Colors } from "../../styles/colors";
import { NavigationActions, StackActions } from "react-navigation";

export default class ActivationMessage extends Component {
  render() {
    return (
      <Container style={{ backgroundColor: "#421B2C" }}>
        {/* <Navbar left={left} right={right} title="LOGIN" /> */}
        <View style={styles.mainContainer}>
          <Thumbnail
            square
            style={{
              width: 180,
              height: 88
            }}
            resizeMode="contain"
            source={Assets.logo}
          />
          <Text style={styles.mainTitle}>
            Your account is not activated yet.
          </Text>
          <Button onPress={this._createAccount} style={styles.button}>
            <Text style={styles.buttonText}>Call Support</Text>
          </Button>
          <Button
            onPress={() =>
              this.props.navigation.dispatch(
                StackActions.reset({
                  index: 0,
                  key: null,
                  actions: [
                    NavigationActions.navigate({ routeName: "loginScreen" })
                  ]
                })
              )
            }
            style={styles.button}
          >
            <Text style={styles.buttonText}>Go Back</Text>
          </Button>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  topcontainer: {
    justifyContent: "center",
    paddingLeft: 30,
    paddingRight: 30,
    marginTop: 20
  },

  bottomcontainer: {
    flex: 1,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingTop: 25,
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: Colors.maroonColor
  },
  mainTitle: {
    marginTop: 15,
    fontSize: 32,
    fontWeight: "normal",
    color: Colors.white
  },

  subTitle: {
    fontSize: 20,
    fontWeight: "normal",
    color: Colors.white
  },
  button: {
    width: 200,
    justifyContent: "center",
    backgroundColor: "white",
    alignSelf: "center",
    marginTop: 20
  },

  buttonText: {
    color: Colors.maroonColor,
    fontWeight: "bold"
  }
});
