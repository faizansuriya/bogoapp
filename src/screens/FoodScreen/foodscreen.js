import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import StoreList from "../../components/listview";

export default class FoodScreen extends Component {
  render() {
    return (
      <View>
        <StoreList {...this.props} />
      </View>
    );
  }
}
