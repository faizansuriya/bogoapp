
import React from "react";
import { View , Text, Left, Button, Icon} from 'native-base';
import {Colors} from "../../styles/colors";
import Navbar from "../../components/navbar";

class PrivacyPolicy extends React.Component{
render(){
    var left = (
        <Left>
            <Button onPress={() => this.props.navigation.pop()} transparent>
                <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
            </Button>
        </Left>
    )
    return(
        <View>
             <Navbar left={left} title="Privacy Policy" />
            <Text>
                Privacy Policy
            </Text>
        </View>
    )
}
}
export default PrivacyPolicy;
