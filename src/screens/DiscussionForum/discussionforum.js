import React, { Component } from "react";
import {
  Alert,
  StyleSheet,
  FlatList,
  KeyboardAvoidingView,
  Dimensions,
  Image,
  RefreshControl,
  TouchableOpacity
} from "react-native";
import Icons from "react-native-vector-icons/FontAwesome5";
import {
  Container,
  View,
  Text,
  Left,
  Right,
  Icon,
  Button,
  Thumbnail,
  Card,
  CardItem
} from "native-base";
import { Colors } from "../../styles/colors";
import Navbar from "../../components/navbar";
import services from "../../services";
import { bold } from "ansi-colors";
import ImageLoad from "react-native-image-placeholder";

class DiscussionForum extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      refreshing: false
    };
  }

  componentDidMount() {
    this.getAllPost();
  }

  getAllPost = async () => {
    try {
      const getData = await services.auth.getAllPost();
      // ("This is the all posts", getData);
      this.setState({ data: getData.data, selectedCategory: getData.data[0] });
    } catch (error) {
      // (error);
    }
  };

  onSelectItem = (item, index) => {
    this.props.navigation.navigate("TopicPostScreen", {
      post: item,
      refreshList: this.refreshList
    });
  };

  // _keyExtractor(item, index) {
  //   return index;
  // }
  _keyExtractor(index) {
    return index;
  }

  refreshList = () => {
    this.getAllPost();
  };

  renderItem(data) {
    // let { item, index } = data;
    // // ("This is the data post item", item);

    let { item } = data;
    // ("This is the data post item", item);

    return (
      <Card>
        <TouchableOpacity onPress={() => this.onSelectItem(item)}>
          <CardItem>
            <View style={{ flex: 1, flexDirection: "column" }}>
              <View style={styles.itemBlock}>
                {/* <Thumbnail
                  square
                  // source={{ uri: item.media }}
                  // style={styles.itemImage}
                  /> */}
                <ImageLoad
                  style={{
                    width: 80,
                    height: 80
                  }}
                  borderRadius={10}
                  loadingStyle={{ size: "large", color: "gray" }}
                  source={{ uri: item.media }}
                />
                <View style={styles.itemMeta}>
                  <Text style={styles.itemName}>{item.title}</Text>
                  {item.text.length > 60 ? (
                    <Text style={styles.itemLastMessage}>
                      {item.text.substr(0, 100)}
                      {"..."}
                    </Text>
                  ) : (
                    <Text style={styles.itemLastMessage}>{item.text}</Text>
                  )}
                </View>
              </View>

              <View style={styles.itemBlock}>
                <Button transparent>
                  <Icons
                    name="thumbs-up"
                    size={14}
                    style={{ color: Colors.maroonColor }}
                  />
                  <Text style={{ fontSize: 14, color: Colors.maroonColor }}>
                    {item.totalLikes}
                  </Text>
                </Button>
                <Button transparent>
                  <Icons
                    name="comment"
                    size={14}
                    style={{ color: Colors.maroonColor }}
                  />
                  <Text style={{ fontSize: 14, color: Colors.maroonColor }}>
                    {item.totalComments}
                  </Text>
                </Button>
              </View>
            </View>
          </CardItem>
        </TouchableOpacity>
      </Card>
    );
  }

  //   renderSeparator() {
  //     return <View style={styles.separator} />;
  //   }

  //   renderHeader() {
  //     return (
  //       <View style={styles.header}>
  //         <Text style={styles.headerText}>Conversations</Text>
  //       </View>
  //     );
  //   }

  _onRefresh = () => {
    this.setState({
      refreshing: true
    });
    setTimeout(
      function() {
        this.setState({
          refreshing: false
        });
      }.bind(this),
      1000
    );
  };
  render() {
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
        </Button>
      </Left>
    );

    var right = (
      <Right>
        <Button
          onPress={() => {
            this.props.navigation.navigate("AddForumScreen", {
              refreshList: this.refreshList
            });
          }}
          transparent
        >
          <Icon name="md-add" style={{ color: Colors.white }} />
        </Button>
      </Right>
    );
    return (
      <Container>
        <Navbar left={left} right={right} title="Discussion" />
        <View style={styles.container}>
          <FlatList
            // keyExtractor={this._keyExtractor}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.data.data}
            renderItem={this.renderItem.bind(this)}
            // ItemSeparatorComponent={this.renderSeparator.bind(this)}
            // ListHeaderComponent={this.renderHeader}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  itemBlock: {
    flexDirection: "row",
    paddingBottom: 5,
    flex: 1
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 20
  },
  itemMeta: {
    marginLeft: 10,
    justifyContent: "center",
    flex: 1
  },
  itemName: {
    fontSize: 16,
    fontWeight: "bold",
    color: Colors.maroonColor
  },
  itemLastMessage: {
    fontSize: 12,
    color: Colors.black
  },
  separator: {
    height: 0.5,
    width: "80%",
    alignSelf: "center",
    backgroundColor: "#555"
  },
  header: {
    padding: 10
  },
  headerText: {
    fontSize: 30,
    fontWeight: "900"
  }
});

export default DiscussionForum;
