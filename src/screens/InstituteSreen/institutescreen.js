import React from "react";
import {
  Container,
  View,
  Item,
  Text,
  Form,
  Icon,
  Picker,
  Card,
  CardItem,
  Thumbnail
} from "native-base";
import {
  StyleSheet,
  TouchableOpacity,
  NativeModules,
  ScrollView,
  Image,
  TextInput
} from "react-native";

import _ from "lodash";
import ImagePicker from "react-native-image-picker";
import Autocomplete from "react-native-autocomplete-input";
import { NavigationActions, StackActions } from "react-navigation";
// import ImagePicker from "react-native-image-crop-picker";
import Snackbar from "react-native-snackbar";
var validate = require("validate.js");
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import { instituteConstraint } from "../../utils/constraints";
import MainBackground from "../../components/MainBackground";
import Logo from "../../components/logo";
import { String } from "../../utils/strings";
import { Colors } from "../../styles/colors";
import { Assets } from "../../../assets";
import services, { updateHeaders } from "../../services";
import * as authUtil from "../../utils/auth.util";
import * as userUtil from "../../utils/user.util";
import loader from "../../components/loader";

const API = "https://swapi.co/api";
const ROMAN = ["", "I", "II", "III", "IV", "V", "VI", "VII"];

// var ImagePicker = NativeModules.ImageCropPicker;

// const DEVICE_WIDTH = Dimensions.get("window").width;
// const DEVICE_HEIGHT = Dimensions.get("window").height;

const options = {
  title: "Select Image",
  allowsEditing: true,
  //customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

class Institute extends React.Component {
  static renderFilm(film) {
    const { title, director, opening_crawl, episode_id } = film;
    const roman = episode_id < ROMAN.length ? ROMAN[episode_id] : episode_id;

    return (
      <View>
        <Text style={styles.titleText}>
          {roman}. {title}
        </Text>
        <Text style={styles.directorText}>({director})</Text>
        <Text style={styles.openingText}>{opening_crawl}</Text>
      </View>
    );
  }
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      images: null,
      institute: "",
      front: null,
      back: null,
      studyYear: undefined,
      courseLength: undefined,
      names: [
        { title: "Sir Syed University of Engineering & Technology" },
        { title: "NED University of Engineering & Technology" },
        { title: "Iqra University" }
      ]
    };
  }

  _yearofstudy = (value, token) => {
    if (value === "default") {
      Snackbar.show({
        title: "Please Select Year Of Study",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      this.setState({
        studyYear: value
      });
    }
  };

  _courselength = (value, token) => {
    if (value === "default") {
      Snackbar.show({
        title: "Please Select Course Length",
        duration: Snackbar.LENGTH_LONG
      });
    } else {
      this.setState({
        courseLength: value
      });
    }
  };

  findFilm(institute) {
    if (institute === "") {
      return [];
    }

    const { names } = this.state;
    const regex = new RegExp(`${institute.trim()}`, "i");
    return names.filter(film => film.title.search(regex) >= 0);
  }

  _submit = async () => {
    // var constraints = {
    //   institute: instituteConstraint
    // };
    // var validation = validate(
    //   { institute: this.state.institute },
    //   constraints,
    //   { format: "flat" }
    // );
    //   // (validation);
    // if (validation) {
    //   Snackbar.show({
    //     title: validation[0],
    //     duration: Snackbar.LENGTH_LONG
    //   });
    // alert(validation[0]);
    //   return;
    // }

    if (
      this.state.front === null ||
      this.state.back === null ||
      this.state.studyYear === undefined ||
      this.state.courseLength === undefined ||
      this.state.institute === ""
    ) {
      Snackbar.show({
        title: "All fields are required",
        duration: Snackbar.LENGTH_LONG
      });
      return;
    }

    if (_.map(this.state.names, "title").indexOf(this.state.institute) < 0) {
      Snackbar.show({
        title: "Select Institute from the given option",
        duration: Snackbar.LENGTH_LONG
      });
      return;
    }

    try {
      const formData = new FormData();
      formData.append("front", {
        uri: this.state.front.uri,
        name: "front",
        type: "image/jpeg"
      });
      formData.append("back", {
        uri: this.state.back.uri,
        name: "back",
        type: "image/jpeg"
      });

      formData.append("studyYear", this.state.studyYear);
      formData.append("courseLength", this.state.courseLength);
      formData.append("institute", this.state.institute);

      const payload = { ...this.state };
      // ("hecho===>", payload);
      // ("formData", formData);
      const { data } = await services.auth.updateInstitute(formData);
      await userUtil.setUserInfo(data);
      if (data.activated) {
        setTimeout(() => {
          this.props.navigation.dispatch(
            StackActions.reset({
              index: 0,
              key: null,
              actions: [NavigationActions.navigate({ routeName: "Home" })]
            })
          );
        }, 2000);
      } else {
        loader.show(
          "Registration successfully complete, Your account will be activated after further processing"
        );
        setTimeout(() => {
          authUtil.logout();
          this.props.navigation.dispatch(
            StackActions.reset({
              index: 0,
              key: null,
              actions: [
                NavigationActions.navigate({ routeName: "loginScreen" })
              ]
            })
          );
        }, 2000);
      }
    } catch (error) {
      Snackbar.show({
        title: error.response.data.message,
        duration: Snackbar.LENGTH_LONG
      });
    }
  };
  _uploadImage = image => {
    ImagePicker.showImagePicker(options, response => {
      // ("Response = ", response);

      if (response.didCancel) {
        // ("User cancelled image picker");
      } else if (response.error) {
        // ("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        // ("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response.uri };
        // ("This is image source>>>", source);

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        if (image === "first") {
          this.setState({
            front: source
          });
        } else {
          this.setState({
            back: source
          });
        }
      }
    });
  };

  onValueChange(item) {
    console.log(item);

    this.setState({ institute: item });
  }

  render() {
    const { institute } = this.state;
    const names = this.findFilm(institute);
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    return (
      <MainBackground>
        <KeyboardAwareScrollView>
          <Container style={{ backgroundColor: "transparent" }}>
            <View style={styles.mainContainer}>
              <View style={styles.topcontainer}>
                <View style={styles.logocontainer}>
                  <Logo />
                </View>
              </View>
              <View style={styles.bottomcontainer}>
                <View style={{ marginBottom: 25 }}>
                  <Text style={styles.mainTitle}>Almost Ready,</Text>
                  <Text style={styles.subTitle}>
                    Please provide your educational information
                  </Text>
                </View>
                <View
                  style={
                    {
                      // width: "100%",
                      // display: "flex",
                      // flexWrap: "nowrap",
                      // backgroundColor: "tranparent",
                      // height: this.state.input ? 200 : 50
                    }
                  }
                >
                  <Item picker>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ width: undefined }}
                      placeholder="Select Institute"
                      placeholderStyle={{ color: "#bfc6ea" }}
                      placeholderIconColor="#007aff"
                      selectedValue={this.state.institute}
                      onValueChange={this.onValueChange.bind(this)}
                    >
                      {this.state.names.map((item, index) => {
                        return (
                          <Picker.Item
                            key={item.title}
                            label={item.title}
                            value={item.title}
                          />
                        );
                      })}
                    </Picker>
                  </Item>
                </View>

                <View>
                  <Form>
                    <Picker
                      style={{
                        width: "100%",
                        backgroundColor: "transparent",
                        color: "#b0b8bd"
                        // display: this.state.input ? "none" : "flex"
                      }}
                      selectedValue={this.state.studyYear}
                      onValueChange={this._yearofstudy}
                    >
                      <Picker.Item label="Year Of Admission" value="default" />
                      <Picker.Item label="2014" value="2014" />
                      <Picker.Item label="2015" value="2015" />
                      <Picker.Item label="2016" value="2016" />
                      <Picker.Item label="2017" value="2017" />
                      <Picker.Item label="2018" value="2018" />
                    </Picker>
                  </Form>
                  <View
                    style={{
                      backgroundColor: Colors.white,
                      alignSelf: "stretch",
                      height: 1,
                      marginBottom: 10
                    }}
                  />
                </View>
                <View>
                  <Form>
                    <Picker
                      style={{
                        width: "100%",
                        backgroundColor: "transparent",
                        color: "#b0b8bd"
                        // display: this.state.input ? "none" : "flex"
                      }}
                      selectedValue={this.state.courseLength}
                      onValueChange={this._courselength}
                    >
                      <Picker.Item
                        label="Select Course Length"
                        value="default"
                      />
                      <Picker.Item label="1" value="1" />
                      <Picker.Item label="2" value="2" />
                      <Picker.Item label="3" value="3" />
                      <Picker.Item label="4" value="4" />
                      <Picker.Item label="5" value="5" />
                    </Picker>
                  </Form>
                  <View
                    style={{
                      backgroundColor: Colors.white,
                      alignSelf: "stretch",
                      height: 1,
                      marginBottom: 10
                    }}
                  />
                </View>

                <Text
                  style={{
                    textAlign: "center",
                    color: "white",
                    fontSize: 18,
                    paddingTop: 10,
                    paddingBottom: 10
                  }}
                >
                  Upload ID Card Image
                </Text>

                <View
                  style={{
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      marginRight: 5,
                      alignItems: "center"
                    }}
                  >
                    {this.state.front ? (
                      <TouchableOpacity
                        style={styles.uploadImage}
                        onPress={this._uploadImage.bind(this, "first")}
                      >
                        <Image
                          source={this.state.front}
                          style={styles.uploadImage}
                        />
                      </TouchableOpacity>
                    ) : (
                      <TouchableOpacity
                        style={styles.uploadImage}
                        onPress={this._uploadImage.bind(this, "first")}
                      >
                        <Thumbnail
                          square
                          resizeMode="contain"
                          source={Assets.frontImage}
                          style={styles.uploadImage}
                        />
                      </TouchableOpacity>
                    )}
                    <Text
                      style={{
                        paddingTop: 10,
                        paddingBottom: 10,
                        textAlign: "center",
                        justifyContent: "center",
                        color: "#ffcf31"
                      }}
                    >
                      Front ID Card
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      marginLeft: 5,
                      alignItems: "center"
                    }}
                  >
                    {this.state.back ? (
                      <TouchableOpacity
                        style={styles.uploadImage}
                        onPress={this._uploadImage.bind(this, "second")}
                      >
                        <Image
                          source={this.state.back}
                          style={styles.uploadImage}
                        />
                      </TouchableOpacity>
                    ) : (
                      <TouchableOpacity
                        style={styles.uploadImage}
                        onPress={this._uploadImage.bind(this, "second")}
                      >
                        <Thumbnail
                          square
                          resizeMode="contain"
                          source={Assets.backImage}
                          style={styles.uploadImage}
                        />
                      </TouchableOpacity>
                    )}
                    <Text
                      style={{
                        paddingTop: 10,
                        paddingBottom: 10,
                        textAlign: "center",
                        justifyContent: "center",
                        color: Colors.white
                      }}
                    >
                      Back ID Card
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    alignSelf: "stretch"
                  }}
                >
                  <TouchableOpacity
                    onPress={this._submit}
                    style={styles.button}
                  >
                    <Text style={styles.buttonText}>{String.submit}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Container>
        </KeyboardAwareScrollView>
      </MainBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: "center"
    // alignItems: "center",
  },

  topcontainer: {
    justifyContent: "center",
    paddingLeft: 30,
    paddingRight: 30,
    marginTop: 20
  },

  bottomcontainer: {
    flex: 1,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    paddingTop: 25,
    paddingLeft: 30,
    paddingRight: 30,
    backgroundColor: Colors.formBackground
  },

  logocontainer: {
    alignItems: "center",
    marginBottom: 20,
    marginTop: 10
  },
  mainTitle: {
    fontSize: 22,
    fontWeight: "bold",
    textAlign: "left",
    width: "100%",
    color: Colors.white
  },

  subTitle: {
    fontSize: 14,
    textAlign: "left",
    width: "100%",
    color: Colors.white
  },

  smallcontent: {
    fontSize: 16,
    textAlign: "left",
    width: "100%",
    color: Colors.content
  },

  button: {
    // marginTop: 10,
    alignSelf: "stretch",
    height: 45,
    borderRadius: 10,
    backgroundColor: Colors.buttonBackground,
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: Colors.buttonText,
    fontWeight: "bold",
    fontSize: 16
  },
  uploadImage: {
    width: 70,
    height: 70
  },

  autoinputcontainer: {
    flex: 1,
    paddingTop: 25
  },

  autocompleteContainer: {
    width: "100%",
    display: "flex"
  },
  itemText: {
    fontSize: 15,
    margin: 2
  },
  descriptionContainer: {
    // `backgroundColor` needs to be set otherwise the
    // autocomplete input will disappear on text input.
    backgroundColor: "green",
    marginTop: 25
  },
  infoText: {
    textAlign: "center"
  },
  titleText: {
    fontSize: 18,
    fontWeight: "500",
    marginBottom: 10,
    marginTop: 10,
    textAlign: "center"
  },
  directorText: {
    color: "grey",
    fontSize: 12,
    marginBottom: 10,
    textAlign: "center"
  },
  openingText: {
    textAlign: "center"
  },
  picker: {
    width: 200,
    height: 44,
    backgroundColor: "#FFF0E0",
    borderColor: "red",
    borderBottomWidth: 2,
    flex: 90
  },

  pickerItem: {
    height: 44,
    color: "red"
  }
});

export default Institute;
