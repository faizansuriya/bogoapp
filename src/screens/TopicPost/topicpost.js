import React from "react";
import {
  Container,
  View,
  Card,
  CardItem,
  Left,
  Right,
  Button,
  Icon,
  Item,
  Input,
  Text,
  Thumbnail
} from "native-base";
import PostComment from "../../components/comments/comments";
import CommentInput from "../../components/comments/input";

import {
  StyleSheet,
  ScrollView,
  AsyncStorage,
  FlatList,
  RefreshControl
} from "react-native";
import Icons from "react-native-vector-icons/FontAwesome";
import Logo from "../../components/logo";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import Snackbar from "react-native-snackbar";
import MainBackground from "../../components/MainBackground";
import services from "../../services";
import * as authUtil from "../../utils/auth.util";
import { get } from "lodash";
import { emailConstraint, passwordConstraint } from "../../utils/constraints";
var validate = require("validate.js");
import { String } from "../../utils/strings";
import { Colors } from "../../styles/colors";
import { Assets } from "../../../assets";
import Navbar from "../../components/navbar";
import * as userUtil from "../../utils/user.util";
import ImageLoad from "react-native-image-placeholder";

class TopicPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      comments: [],
      refreshing: false,
      likes: this.props.navigation.state.params.post.totalLikes,
      likeBy: [],
      liked: -1
    };
  }

  componentDidMount() {
    this._fetchComments();
    this._getPost();
  }

  _getPost = async postId => {
    try {
      const getData = await services.auth.getPostById(
        this.props.navigation.state.params.post._id
      );
      // ("This is get Data of get post", getData.data.likedBy);
      const getUser = await userUtil.getUserInfo();
      // ("This is getUser", getUser);
      let result = getData.data.likedBy.indexOf(getUser._id);
      // ("This is getpost result", result);
      this.setState({
        data: getData.data,
        likes: getData.data.totalLikes,
        liked: result
      });
    } catch (error) {
      // (error);
    }
  };

  _fetchComments = async postId => {
    try {
      const getData = await services.auth.getCommentByPost(
        this.props.navigation.state.params.post._id
      );
      this.setState({ comments: getData.data });
    } catch (error) {
      // (error);
    }
  };

  _likePost = async postId => {
    try {
      const getLikes = await services.auth.likePost(
        this.props.navigation.state.params.post._id
      );
      // ("This is like Api Hit", getLikes.data.likedBy);
      this.setState(
        { likes: getLikes.data.totalLikes, likeBy: getLikes.data.likedBy },
        () => {
          this.props.navigation.state.params.refreshList();
        }
      );
      const getData = await userUtil.getUserInfo();
      // ("This is the _id", getData._id);
      let result = this.state.likeBy.indexOf(getData._id);
      // ("This is likeby result", result);
      this.setState({ liked: result });
    } catch (error) {
      // (error);
    }
  };

  _onSubmit = async text => {
    try {
      const getData = await services.auth.postComment({
        postId: this.props.navigation.state.params.post._id,
        text: text,
        date: (new Date() / 1000) * 1000
      });
      const listComments = await services.auth.getCommentByPost(
        this.props.navigation.state.params.post._id
      );
      this.setState({ comments: listComments.data }, () => {
        this.props.navigation.state.params.refreshList();
      });
      // ("This is the comment Id", listComments);
    } catch (error) {
      // (error);
    }
  };

  // _keyExtractor(item, index) {
  //   return index;
  // }

  _keyExtractor(index) {
    return index;
  }

  renderItem = comments => {
    let { item, index } = comments;
    // ("This is the item", item);
    return (
      <View style={styles.commentcontainer}>
        <PostComment comment={item} key={index} />
      </View>
    );
  };

  //   renderSeparator() {
  //     return <View style={styles.separator} />;
  //   }

  renderHeader = () => {
    return (
      <View>
        <View style={{ flexDirection: "column" }}>
          <View style={styles.itemBlock}>
            {/* <Thumbnail
              square
              source={{
                uri: this.state.data.thumb
              }}
              style={styles.itemImage}
            /> */}
            <ImageLoad
              style={{
                width: 80,
                height: 80
              }}
              borderRadius={10}
              loadingStyle={{ size: "large", color: "gray" }}
              source={{ uri: this.state.data.thumb }}
            />

            <View style={styles.itemMeta}>
              <Text style={styles.itemName}>{this.state.data.title}</Text>
              <Text style={styles.time}>{this.state.data.Time}</Text>
            </View>
          </View>
        </View>
        <View style={styles.description}>
          <Text style={styles.summary}>{this.state.data.text}</Text>
        </View>
        <View>
          {/* <Thumbnail
            square
            source={{
              uri: this.state.data.thumb
            }}
            style={styles.mainImage}
          /> */}
          <ImageLoad
            style={{
              width: "100%",
              height: 220
            }}
            borderRadius={10}
            loadingStyle={{ size: "large", color: "gray" }}
            source={{
              uri: this.state.data.thumb
            }}
          />
        </View>
        <View style={styles.itemBlock}>
          <Button
            onPress={() => {
              this._likePost();
            }}
            transparent
          >
            {this.state.liked >= 0 ? (
              <Icons name="thumbs-up" size={16} style={styles.iconStyle} />
            ) : (
              <Icons name="thumbs-o-up" size={16} style={styles.iconStyle} />
            )}

            <Text style={{ fontSize: 16, color: Colors.maroonColor }}>
              {this.state.likes}
            </Text>
          </Button>
          <Button transparent>
            <Icons name="comment" size={16} style={styles.iconStyle} />
            <Text style={{ fontSize: 16, color: Colors.maroonColor }}>
              {this.state.comments.length}
            </Text>
          </Button>
        </View>
      </View>
    );
  };
  renderFooter = () => {
    return <CommentInput onSubmit={this._onSubmit} />;
  };
  _onRefresh() {
    this.setState({
      refreshing: true
    });
    setTimeout(
      function() {
        this.setState({
          refreshing: false
        });
      }.bind(this),
      1000
    );
  }

  render() {
    const { comments } = this.state;
    var left = (
      <Left>
        <Button onPress={() => this.props.navigation.pop()} transparent>
          <Icon name="ios-arrow-back" style={{ color: Colors.white }} />
        </Button>
      </Left>
    );
    return (
      <Container style={{ opacity: 0.9 }}>
        <Navbar left={left} title="Topic Post" />
        <View style={styles.container}>
          <FlatList
            //  keyExtractor={this._keyExtractor}
            keyExtractor={(item, index) => index.toString()}
            data={comments}
            renderItem={this.renderItem.bind(this)}
            // ItemSeparatorComponent={this.renderSeparator.bind(this)}
            ListHeaderComponent={this.renderHeader}
            // ListFooterComponent={this.renderFooter}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
            }
          />
        </View>
        <CommentInput onSubmit={this._onSubmit} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  itemBlock: {
    flexDirection: "row",
    paddingBottom: 5
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 20
  },
  mainImage: {
    width: "100%",
    height: 220
  },
  itemMeta: {
    marginLeft: 10,
    justifyContent: "center",
    flex: 1
  },
  iconStyle: {
    color: Colors.maroonColor
  },
  itemName: {
    fontSize: 20,
    fontWeight: "bold",
    color: Colors.maroonColor
  },
  time: {
    fontSize: 12,
    color: Colors.maroonColor
  },
  summary: {
    fontSize: 15,
    color: Colors.black,
    marginBottom: 15
  },
  separator: {
    height: 0.5,
    width: "80%",
    alignSelf: "center",
    backgroundColor: "#555"
  },
  header: {
    padding: 10
  },
  headerText: {
    fontSize: 30,
    fontWeight: "900"
  },

  commentcontainer: {
    flex: 1
    //paddingTop: 5
  }
});

export default TopicPost;
