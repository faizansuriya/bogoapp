import { post, get, put, remove } from "./httpProvider";
import _ from "lodash";

const SERVICE_URLS = _.mapValues(
  {
    login: "users/login",
    register: "users/signup",
    verification: "users/verify",
    institute: "users/updateinstitute",
    categories: "categories/get",
    revealCode: "code/reveal",
    getPremiumVendors: "vendors/premium",
    getAllPost: "forum/all",
    postComment: "comments/add",
    addForum: "forum/add",
    uploadImage: "vendors/upload",
    getUniversity: "universities/get",
    updateProfile: "users/update",
    search: "vendors/search",
    updateHitCount: "vendors/updatehitcount"
  },
  _.template
);

const login = ({ email, password }) =>
  post(SERVICE_URLS.login(), { email, password });

const registerUser = ({ firstName, lastName, email, mobile, password }) =>
  post(SERVICE_URLS.register(), {
    firstName,
    lastName,
    email,
    mobile,
    password
  });

const getImageUrl = body => post(SERVICE_URLS.uploadImage(), body);

const postComment = ({ postId, text, date }) =>
  post(SERVICE_URLS.postComment(), { post: postId, text, date });

const search = data => post(SERVICE_URLS.search(), data);

const addPost = body => post(SERVICE_URLS.addForum(), body);

const getAllPost = () => get(SERVICE_URLS.getAllPost());

const verifyCode = ({ smsCode }) =>
  post(SERVICE_URLS.verification(), { smsCode });

const updateInstitute = body => post(SERVICE_URLS.institute(), body);
const revealCode = body => post(SERVICE_URLS.revealCode(), body);

const getCategories = () => get(SERVICE_URLS.categories());
const getPremiumVendors = () => get(SERVICE_URLS.getPremiumVendors());

const getVendorsByCategory = categoryId => {
  // ("categoryId:");
  return get("vendors/getbycategory/" + categoryId);
};

const getPostById = postId => {
  return get("forum/get/" + postId);
};

const getCommentByPost = postId => {
  return get("comments/get/" + postId);
};

const likePost = postId => {
  return post("forum/" + postId + "/like");
};

const getAllUniversities = () => get(SERVICE_URLS.getUniversity());

const updateProfile = data => post(SERVICE_URLS.updateProfile(), data);
const updateHitCount = data => post(SERVICE_URLS.updateHitCount(), data);

const authServices = {
  login,
  getCategories,
  registerUser,
  verifyCode,
  updateInstitute,
  getVendorsByCategory,
  revealCode,
  getPremiumVendors,
  getAllPost,
  getCommentByPost,
  postComment,
  addPost,
  getImageUrl,
  likePost,
  getPostById,
  getAllUniversities,
  updateProfile,
  search,
  updateHitCount
  // getCategories
  //   forgetPassword,
  //   resetPassword
  // fetchForgetPassword
};

export default authServices;

// // axios
// function httpGet({ url, options, qs = ''}) {
//     return axios.get(`${url}?${qs`});
// }

// request
// function httpGet({ url, options, qs = ''}) {
//     return request(url', function (error, response, body) {
//         if (!error) {
//             return response;
//         }
//         // ('error:', error); // Print the error if one occurred
//         // ('statusCode:', response && response.statusCode); // Print the response status code if a response was received
//         // ('body:', body); // Print the HTML for the Google homepage.
//     });
// }

// function httpGet(params) {
//     const { url, options, qs = ''} = params;
//     const headerOpts = {
//         'Accept' : 'application/json',
//         'Content-Type' : 'application/json',
//         ...options
//     }

//     return fetch (`${url}?${qs}` , {
//             method : 'GET',
//             headers: headerOpts
//         })

//         .then((response) => response.json())
//         .then ((res) => res)
//         .catch((err) => {
//             // (err)
//             // alert(err)
//         });
// }

// function httpPost(params) {
//   const { url, body, options } = params;
//   const headerOpts = {
//     Accept: "application/json",
//     "Content-Type": "application/json",
//     ...options
//   };

//   return fetch(url, {
//     method: "POST",
//     headers: headerOpts,
//     body: body
//   })
//     .then(response => response.json())
//     .then(res => res)
//     .catch(err => {
//       // (err);
//       // alert(err)
//     });
// }

// function getUsersList({ qs }) {
//     return httpGet({
//         url: `${baseUrl}/users`,
//     })
// }

// function fetchForgetPassword({ email }) {
//   return httpPost({
//     url: `${BASE_URL}/forgot_password`,
//     body: JSON.stringify({
//       //type: 'normal',
//       email: email
//       //password: this.state.password,
//     })
//   });
// }
