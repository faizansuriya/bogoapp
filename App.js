import React from "react";
import { YellowBox, View } from "react-native";
import Splash from "./src/screens/SplashScreen";
import { RootStack } from "./src/routes";
YellowBox.ignoreWarnings(['Require cycle:']);

class App extends React.Component {
  state = {
    fontsAreLoaded: false
  };

  // _hideSplashScreen = () => {
  //   this.setState(() => ({ showSplash: false }));
  // };

  componentDidMount() {
    this.setState({ fontsAreLoaded: true });
  }

  render() {
    if (!this.state.fontsAreLoaded) {
      return <View />;
    }

    return <RootStack />;
  }
}

export default App;
